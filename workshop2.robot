***Settings***
Library  BuiltIn
Library  SeleniumLibrary

**Variables**
${url}=  https://ptt-devpool-robotframework.herokuapp.com
${id}=  admin
${pass}=  1234

&{a}=   firstname= David   lastname= Beckham  email= David.Beckham@gmail.com
&{b}=   firstname= Johny   lastname= Depp  email= Johny.Depp@gmail.com
&{c}=   firstname= Robert   lastname= Downey  email= Robert.Downey@gmail.com
&{d}=   firstname= Tom   lastname= Hanks  email= Tom.Hanks@gmail.com

@{infolist}=  a  b  c  d 

**Test Cases**
เปิด browser
    Open Browser  browser=Chrome
    Maximize Browser Window
    Set Selenium Speed  0.5
ไปที่เว็บ
    Go To  ${url}
พิมพ์ username,password
    Input Text  id=txt_username  ${id}
    Input Text  id=txt_password  ${pass}
กดปุ่ม sign in
    Click Button  id=btn_login
กดปุ่ม new data
    Click Button  id=btn_new
เพิ่ม data
    FOR  ${i}  IN  @{infolist}
        Test  &{${i}}
        Click Button  id=btn_new
    END
    Click Button  id=btn_new_close

7. Screenshot
    Capture Page Screenshot  filename=${CURDIR}/screenshot.png

*** Keywords ***
Test
    [Arguments]    &{para}
    FOR    ${key}  ${value}   IN    &{para}
            Run Keyword If  '${key}' == 'firstname'   Input Text  id=txt_new_firstname  ${para["${key}"]}
            Run Keyword If  '${key}' == 'lastname'   Input Text  id=txt_new_lastname  ${para["${key}"]}
            Run Keyword If  '${key}' == 'email'   Input Text  id=txt_new_email  ${para["${key}"]}
    END
    Click Button  id=btn_new_save
